module gitee.com/info-superbahn-ict/nervousgo

go 1.15

require (
	github.com/Shopify/sarama v1.28.0
	github.com/satori/go.uuid v1.2.0
)
