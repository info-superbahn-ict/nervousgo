package nervous_go

type Message interface {
}


type Controller interface {
	Subscribe(topic string) error
	Receive(topic string) (Message, error)
	Send(topic string, key string, value string) error

	RPCRegister(registerName string, rpcProcess func(args ...interface{}) (interface{}, error)) error
	RPCRemove(registerName string) error

	RPCCall(targetGuid string, funcName string, params ...interface{}) (interface{}, error)
	RPCCallCustom(targetGuid string, tryTime int, tryInterval int, funcName string, params ...interface{}) (interface{}, error)
	RPCList() ([]string, error)
	RPCContains(funcName string) (bool, error)

	Run() error
	Close() error
}

