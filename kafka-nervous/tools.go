package kafka_nervous
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)
func ReadJsonFile(address string)(map[string]interface{},error){
	configFile, err := ioutil.ReadFile(address)
	if err != nil {
		return nil, fmt.Errorf("open file %v",err)
	}
	var configContent map[string]interface{}
	err = json.Unmarshal(configFile, &configContent)
	if err != nil {
		return nil, fmt.Errorf("unmarshal file %v",err)
	}
	return configContent, nil
}
const ResponseTopicPrefix = "RES"
const RequestTopicPrefix = "REQ"

func GenerateRequestTopic(guid string)string{
	return RequestTopicPrefix + guid
}

func GenerateResponseTopic(guid string)string{
	return ResponseTopicPrefix + guid
}
