package kafka_nervous

import (
	"context"
	"fmt"
	Ng "gitee.com/info-superbahn-ict/nervousgo"
	"github.com/Shopify/sarama"
	"github.com/satori/go.uuid"
	"sync"
	"time"
)

const TryTime = 5
const TryInterval = 500


type nervousMessage struct {
	Topic string
	Key   string
	Value string
}

func NewNervousMessage() Ng.Message {
	return &nervousMessage{}
}



type nervous struct {
	ctx                context.Context
	cancel             context.CancelFunc
	guid               string
	responseTopic      string
	requestTopic       string
	producerLock       sync.Mutex
	producer           sarama.SyncProducer
	consumer           sarama.Consumer
	partitionConsumers sync.Map
	processMap         sync.Map
	messageMap         sync.Map
	offsetPolicy       int64
}

/*
初始化总线
@param 上下文，配置文件目录，全局id
@return 总线指针，错误
*/
func NewNervous(ctx context.Context, configAddress string, guid string) (Ng.Controller, error) {
	//TODO guid
	n := &nervous{}
	n.ctx, n.cancel = context.WithCancel(ctx)
	n.guid = guid

	//read nervous_config.json
	configContent, err := ReadJsonFile(configAddress)
	if err != nil {
		return nil, fmt.Errorf("open file %v\n", err)
	}

	if configContent["OFFSET_RESET"] == "earliest" {
		n.offsetPolicy = sarama.OffsetOldest
	} else {
		n.offsetPolicy = sarama.OffsetNewest
	}

	//create producer
	config := sarama.NewConfig()
	config.ClientID = guid + "_client"
	config.Producer.Return.Successes = true
	config.Version = sarama.V2_0_0_0

	n.producer, err = sarama.NewSyncProducer([]string{configContent["KAFKA_BROKERS"].(string)}, config)
	if err != nil {
		return nil, fmt.Errorf("create producer %v\n", err)
	}

	//create consumer
	n.consumer, err = sarama.NewConsumer([]string{configContent["KAFKA_BROKERS"].(string)}, sarama.NewConfig())
	if err != nil {
		return nil, fmt.Errorf("create consumer %v\n", err)
	}

	return n, nil
}

/*
向启动客户端和服务端
@param 注册参数
@return 错误
*/
func (n *nervous) Run() error {
	//TODO
	if err:=n.StartRPCServer();err !=nil{
		return err
	}

	if err:=n.StartRPCClient();err !=nil{
		return err
	}
	return nil
}


/*
向Sponge注册，获取guid
@param 注册参数
@return 错误
*/
func (n *nervous) RegisterGuid(jsonString string) error {
	n.guid = "testguid"
	//TODO
	return nil
}

/*那是不是需要确定博士论文的
订阅topic
@param 要订阅的topic
@return 错误
*/
func (n *nervous) Subscribe(topic string) error {
	if _, exist := n.partitionConsumers.Load(topic); exist {
		return fmt.Errorf("topic already exist\n")
	} else {
		partitionConsumer, err := n.consumer.ConsumePartition(topic, 0, n.offsetPolicy)
		if err != nil {
			return fmt.Errorf("create PartitionConsumer %v\n", err)
		}
		n.partitionConsumers.Store(topic, partitionConsumer)
	}
	return nil
}

/*
取消订阅topic
@param 要取消订阅的topic
@return 错误
*/
func (n *nervous) Unsubscribe(topic string) error {
	var toClose interface{}
	var exist bool
	if toClose, exist = n.partitionConsumers.Load(topic); !exist {
		return fmt.Errorf("remove topic: %s not exist\n", topic)
	}
	if err := toClose.(sarama.PartitionConsumer).Close(); err != nil {
		return fmt.Errorf("close PartitionConsumer fail %v\n", err)
	}
	n.partitionConsumers.Delete(topic)
	return nil
}

/*
从某个topic获取一条消息
@param 要获取消息的topic
@return 获得的消息，错误
*/
func (n *nervous) Receive(topic string) (Ng.Message, error) {
	partitionConsumer, exist := n.partitionConsumers.Load(topic)
	if !exist {
		if err := n.Subscribe(topic); err != nil {
			return nervousMessage{}, fmt.Errorf("topic %s not exist, and subscribe fail:\n", topic)
		}
	}
	select {
	case tmpMsg := <-partitionConsumer.(sarama.PartitionConsumer).Messages():
		return nervousMessage{Topic: tmpMsg.Topic, Key: string(tmpMsg.Key), Value: string(tmpMsg.Value)}, nil
	case <-n.ctx.Done():
		return nervousMessage{}, fmt.Errorf("nervous closed\n")
	}
}

/*
func (n *nervous)ReceiveList(topic string, waitMs int)([]NervousMessage,error){
	msgList:=[]NervousMessage{}
	timeout:=time.After(time.Millisecond*time.Duration(waitMs))
	partitionConsumer,exist := n.partitionConsumers.Load(topic)
	if !exist{
		if err:=n.Subscribe(topic);err!=nil{
			return msgList,fmt.Errorf("topic %s not exist, and subscribe fail:\n",topic)
		}
	}
	for{
		select {
		case tmpMsg := <-partitionConsumer.(sarama.PartitionConsumer).Messages():
			msgList = append(msgList,NervousMessage{Topic:tmpMsg.Topic,Key:string(tmpMsg.Key),Value:string(tmpMsg.Value)})
		case <-n.ctx.Done():
			return msgList, fmt.Errorf("nervous closed\n")
		case <-timeout:
			return msgList,nil
		}
	}
}
*/

/*
发送消息
@param 发送消息的topic,消息的key（可以为""），消息的json字符串
@return 错误
*/
func (n *nervous) Send(topic string, key string, value string) error {
	if key != "" {
		n.producerLock.Lock()
		_, _, err := n.producer.SendMessage(&sarama.ProducerMessage{Topic: topic, Key: sarama.StringEncoder(key), Value: sarama.StringEncoder(value)})
		n.producerLock.Unlock()
		return err
	} else {
		n.producerLock.Lock()
		_, _, err := n.producer.SendMessage(&sarama.ProducerMessage{Topic: topic, Value: sarama.StringEncoder(value)})
		n.producerLock.Unlock()
		return err
	}
}

/*
关闭Nervous
@param
@return 错误
*/
func (n *nervous) Close() error {
	n.cancel()
	if err := n.producer.Close(); err != nil {
		return fmt.Errorf("close producer %v\n", err)
	}
	n.partitionConsumers.Range(func(k, v interface{}) bool {
		if err := v.(sarama.PartitionConsumer).Close(); err != nil {
			return false
		}
		return true
	})
	if err := n.consumer.Close(); err != nil {
		return fmt.Errorf("close Consumer %v\n", err)
	}
	return nil
}

/*
初始化rpc client，开始监听rpc请求的返回值
@param
@return 错误
*/
func (n *nervous) StartRPCClient() error {
	n.responseTopic = GenerateResponseTopic(n.guid)
	if err := n.Subscribe(n.responseTopic); err != nil {
		return fmt.Errorf("subscribe topic: %s fail\n", n.responseTopic)
	}
	go func() {
		for {
			if msg, err := n.Receive(n.responseTopic); err != nil {
				//fmt.Printf("RPCClient Receive fail %v\n",err)
			} else {
				n.messageMap.Store(msg.(nervousMessage).Key, msg.(nervousMessage).Value)
			}
			select {
			case <-n.ctx.Done():
				return
			default:
				continue
			}
		}
	}()
	return nil
}

/*
发起rpc请求
@param 目标rpc server的guid，调用的函数名，传入的参数
@return 返回值，错误
*/
func (n *nervous) RPCCall(targetGuid string, funcName string, params ...interface{}) (interface{}, error) {
	key := uuid.NewV4().String() + targetGuid
	callJsonStr, err := (&MethodInfo{BaseInfo: BaseInfo{SourceGuid: n.guid, TargetGuid: targetGuid}, RegisterName: funcName, ParamArray: params}).ToJson()
	if err != nil {
		return nil, err
	}
	//fmt.Printf("rcpcall:%s\n",callJsonStr)
	if err := n.Send(GenerateRequestTopic(targetGuid), key, callJsonStr); err != nil {
		return nil, err
	}
	returnInfo := new(ReturnInfo)
	for i := 0; i < TryTime; i++ {
		if returnJson, exist := n.messageMap.Load(key); !exist {
		time.Sleep(time.Duration(TryInterval) * time.Millisecond)
		} else {
			returnInfo.FromJson(returnJson.(string))
			n.messageMap.Delete(key)
			if returnInfo.SourceGuid != targetGuid {
				return nil, fmt.Errorf("warning, key match but sourceid:%s not match targetGuid:%s, delete that msg and do nothing\n", returnInfo.SourceGuid, targetGuid)
			}
			return returnInfo.ReturnValue, nil
		}
	}
	return nil, fmt.Errorf("RPCCall no response received\n")
}

/*
发起rpc请求
@param 目标rpc server的guid，重试的次数，每次重试等待的毫秒数，调用的函数名，传入的参数
@return 返回值，错误
*/
func (n *nervous) RPCCallCustom(targetGuid string, tryTime int, tryInterval int, funcName string, params ...interface{}) (interface{}, error) {
	key := uuid.NewV4().String() + targetGuid
	callJsonStr, err := (&MethodInfo{BaseInfo: BaseInfo{SourceGuid: n.guid, TargetGuid: targetGuid}, RegisterName: funcName, ParamArray: params}).ToJson()
	if err != nil {
		return nil, err
	}
	//fmt.Printf("rcpcall:%s\n",callJsonStr)
	if err := n.Send(GenerateRequestTopic(targetGuid), key, callJsonStr); err != nil {
		return nil, err
	}
	returnInfo := new(ReturnInfo)
	for i := 0; i < tryTime; i++ {
		if returnJson, exist := n.messageMap.Load(key); !exist {
			c:=time.NewTicker(time.Millisecond*time.Duration(tryInterval))
			select{
				case<-c.C:continue
				case<-n.ctx.Done():
	return nil, fmt.Errorf("RPCCallCustom exited unexpectedly\n")
			}
		} else {
			returnInfo.FromJson(returnJson.(string))
			n.messageMap.Delete(key)
			if returnInfo.SourceGuid != targetGuid {
				return nil, fmt.Errorf("warning, key match but sourceid:%s not match targetGuid:%s, delete that msg and do nothing\n", returnInfo.SourceGuid, targetGuid)
			}
			return returnInfo.ReturnValue, nil
		}
	}
	return nil, fmt.Errorf("RPCCallCustom no response received\n")
}

/*
向rcp server注册函数
@param 注册的函数名，注册的函数
@return 错误
*/
func (n *nervous) RPCRegister(registerName string, rpcProcess func(args ...interface{}) (interface{}, error)) error {
	if _, exist := n.processMap.Load(registerName); exist {
		//fmt.Printf("registerName:%s alread exist\n",registerName)
	}
	n.processMap.Store(registerName, rpcProcess)
	return nil
}

func (n *nervous) RPCRemove(registerName string) error{
	return nil
}

/*
查看当前rpc server上所有注册了的函数
@param
@return 函数列表，错误
*/
func (n *nervous) RPCList() ([]string, error) {
	processList := []string{}
	n.processMap.Range(func(k, v interface{}) bool {
		processList = append(processList, k.(string))
		return true
	})
	return processList, nil
}

/*
func (n *nervous)RPCList(targetGuid string)(list.List,error){
	processList := list.New()
	if targetGuid == n.guid{
		n.processMap.Range(func(k, v interface{})bool{
			processList.PushBack(k.(string))
			return true
		})
	}else{
		if returnList,err:=n.RPCCall(targetGuid,"funcList");err!=nil{
			return returnList.(list.List),err
		}
	}
	return *processList,nil
}
*/

/*
查看当前rpc server上是否注册了某个函数
@param 查询的函数名
@return 是否存在，错误
*/
func (n *nervous) RPCContains(funcName string) (bool, error) {
	found := false
	n.processMap.Range(func(k, v interface{}) bool {
		if k == funcName {
			found = true
			return false
		}
		return true
	})
	return found, nil
}

/*
func (n *nervous)RPCContains(targetGuid string, funcName string)(bool,error){
	found:=false
	if targetGuid == n.guid{
		n.processMap.Range(func(k, v interface{})bool{
			if k == funcName{
				found = true
				return false
			}
			return true
		})
	}else{
		if funcList,err:=n.RPCList(targetGuid);err!=nil{
			return false,err
		}else{
			for i:=funcList.Front();i!=nil;i=i.Next(){
				if i.Value.(string) ==funcName{
					found = true
					break
				}
			}
		}
	}
	return found,nil
}*/

/*
初始化rpc web_server，开始监听rpc请求，并注册基础rpc函数
@param
@return 错误
*/
func (n *nervous) StartRPCServer() error {
	n.requestTopic = GenerateRequestTopic(n.guid)
	if err := n.Subscribe(n.requestTopic); err != nil {
		return fmt.Errorf("subscribe topic: %s fail\n", n.requestTopic)
	}

	funcList := func(args ...interface{}) (interface{}, error) {
		return n.RPCList()
	}
	n.RPCRegister("funcList", funcList)

	funcContains := func(args ...interface{}) (interface{}, error) {
		return n.RPCContains(args[0].(string))
	}
	n.RPCRegister("funcList", funcList)
	n.RPCRegister("funcContains", funcContains)
	go func() {
		for {
			if msg, err := n.Receive(n.requestTopic); err != nil {
				//fmt.Printf("RPCServer Receive fail %v\n",err)
				return
			} else {
				go func() {
					tmpMethod := MethodInfo{}
					tmpMethod.FromJson(msg.(nervousMessage).Value)
					//fmt.Printf("rpcCall msg value:%s",msg.Value)
					returnInfo := ReturnInfo{BaseInfo: BaseInfo{SourceGuid: tmpMethod.TargetGuid, TargetGuid: tmpMethod.SourceGuid}}
					if process, exist := n.processMap.Load(tmpMethod.RegisterName); !exist {
						returnInfo.Status = 1
						returnInfo.ReturnValue = "no such func"
					} else {
						if callReturn, err := process.(func(args ...interface{}) (interface{}, error))(tmpMethod.ParamArray...); callReturn == nil || err != nil {
							returnInfo.Status = 2
							returnInfo.ReturnValue = err.Error()
						} else {
							returnInfo.Status = 0
							returnInfo.ReturnValue = callReturn
						}
					}
					returnInfoJson, err := returnInfo.ToJson()
					if err != nil {
						//fmt.Printf("transfer to json fail %v\n",err)
						return
					}
					if err := n.Send(GenerateResponseTopic(tmpMethod.SourceGuid), msg.(nervousMessage).Key, returnInfoJson); err != nil {
						//fmt.Printf("RPC response send fail:%v\n",err)
						return
					}
				}()
			}
			select {
			case <-n.ctx.Done():
				return
			default:
				continue
			}
		}
	}()
	return nil
}
