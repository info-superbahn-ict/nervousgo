package kafka_nervous

import (
	"context"
	"fmt"
	"sync"

	//"os"
	//"os/signal"
	//"syscall"
	"testing"
)


func TestJson(t *testing.T){
	m :=new(MethodInfo)
	m.FromJson("{\"source_guid\":\"ff\",\"target_guid\":\"aa\",\"register_name\":\"rn\",\"parma_array\":[3,\"in\"]}")
	fmt.Println(m.SourceGuid)
	fmt.Println(m.TargetGuid)
	fmt.Println(m.RegisterName)
	fmt.Println(m.ParamArray)
	jsonStr,_:=m.ToJson()
	fmt.Println(string(jsonStr))
}

func TestLocalSendReceive(t *testing.T){
	n,_:= NewNervous(context.Background(),"./nervous_config.json","test917")
	wg:=sync.WaitGroup{}
	wg.Add(1)
	n.Subscribe("test917t")
	go func(){
		msg,_:=n.Receive("test917t")
		fmt.Println(msg.(nervousMessage).Topic)
		fmt.Println(msg.(nervousMessage).Key)
		fmt.Println(msg.(nervousMessage).Value)
		wg.Done()
	}()
	n.Send("test917t","keyf","fff")
	wg.Wait()
	n.Close()
}

func TestLocalRPC(t *testing.T){
	c,_:= NewNervous(context.Background(),"./nervous_config.json","testrpcclient")
	_ = c.Run()
	//s,_:= NewNervous(context.Background(),"./nervous_config.json","testrpcserver")
	//s.StartRPCServer()
	testAdd := func(toAdd ...interface{})(interface{},error){
		fmt.Println("in testAdd")
		return toAdd[0].(float64)+1,nil
	}
	/*testFuncList := func(args ...interface{})(interface{},error){
		return s.RPCList()
	}*/
	_ = c.RPCRegister("testAdd",testAdd)
	//s.RPCRegister("funcList",testFuncList)
	if result,err:=c.RPCCall("testrpcserver","testAdd",2);err!=nil{
		fmt.Printf("mock call error:%v\n",err)
	}else{
		fmt.Printf("result: %d\n",int(result.(float64)))
	}
	if result,err:=c.RPCCall("testrpcserver","funcList");err!=nil{
		fmt.Printf("mock call error:%v\n",err)
	}else{
		fmt.Println("rcp",result)
		var tmpR []interface{}
		tmpR = result.([]interface{})
		for i:=0;i<len(tmpR);i++{
			fmt.Println(tmpR[i].(string))
		}
	}
	fmt.Println("close mock client and web_server")
	c.Close()
}

/*
func TestSend(t *testing.T){
	fmt.Println("send?")
	ctx, cancel := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	k, err := NewNervous(ctx, "./nervous_config.json","")
	if err != nil {
		fmt.Printf("new kafka %v", err)
		return
	}


	err = k.Send("resport","hello world")
	if err != nil {
		fmt.Printf("send %v", err)
		return
	}
}

func TestRecv(t *testing.T){
	ctx, cancel := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	k, err := NewNervous(ctx, "./nervous_config.json","")
	if err != nil {
		fmt.Printf("new kafka %v", err)
		return
	}

	err = k.Subscribe("report")
	if err != nil {
		fmt.Printf("add topic %v", err)
		return
	}

	value,err:=k.Receive("report")
	if err != nil {
		fmt.Printf("recv %v", string(value))
		return
	}
}*/
